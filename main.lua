local love = require("love")
local Object = require("classic")
local flux = require("flux")
local tick = require("tick")

local ini = require("lib/ini")

local util = require("lib/util")
local Rocket = require("lib/rocket")

local buildTheme = require("lib/theme")

local rocket

local LOVE = Object:extend()

function LOVE:new(arg)
  for i, a in ipairs(arg) do
    print(i, a)
  end

  local conf = love.iniFile:toTable()

  local rulePath = "rules.ini"

  local rules = {}

  if (love.filesystem.getInfo(rulePath)) then
    local ruleIni = ini.new()
    ruleIni:decode(love.filesystem.lines(rulePath))

    for name, section in ruleIni:sections() do
      table.insert(rules, { glob = name, command = section:get("command") })
    end
  end

  -- print(love.filesystem.getUserDirectory())
  -- print(love.filesystem.getSaveDirectory())
  -- print(love.filesystem.getRequirePath())

  love.graphics.setDefaultFilter("nearest", "nearest", 1)
  love.graphics.setLineStyle("rough")
  love.window.setDisplaySleepEnabled(true)
  love.mouse.setVisible(false)

  love.keyboard.setKeyRepeat(false)
  love.keyboard.setTextInput(true)

  -- needs to be read from love, as the conf setting is merely a suggestion
  local windowWidth, windowHeight = love.window.getMode()

  local canvasWidth, canvasHeight = conf.canvasWidth, conf.canvasHeight

  self.rotation = 0

  if (conf.rotation == 1 or conf.rotation == 3) then
    local ww = windowWidth
    local wh = windowHeight

    windowWidth = wh
    windowHeight = ww

    canvasWidth = conf.canvasHeight
    canvasHeight = conf.canvasWidth
  end

  local rotAdjX, rotAdjY = 0, 0

  if (conf.rotation == 1) then
    rotAdjX = windowHeight
    self.rotation = math.rad(90)
  elseif (conf.rotation == 3) then
    rotAdjY = windowWidth
    self.rotation = math.rad(270)
  elseif (conf.rotation == 2) then
    rotAdjY = windowHeight
    rotAdjX = windowWidth
    self.rotation = math.rad(180)
  end

  self.scaleX = math.floor(windowWidth / canvasWidth)
  self.scaleY = math.floor(windowHeight / canvasHeight)

  self.canvas = love.graphics.newCanvas(canvasWidth, canvasHeight)
  self.canvas:setFilter("nearest", "nearest")

  -- integer scaled canvas
  local integerWidth = canvasWidth * self.scaleX
  local integerHeight = canvasHeight * self.scaleY

  if (conf.scalingType == 'sharp') then
    self.wScaleX = windowWidth / integerWidth
    self.wScaleY = windowHeight / integerHeight

    self.wPosX = rotAdjX
    self.wPosY = rotAdjY

    -- if scale values are equal it means linear scaling on top of integer scaling is not needed
    -- and wPosX / Y are always zero (not counting rotation adjustments).
    if (self.wScaleX ~= self.scaleX or self.wScaleY ~= self.scaleY) then
      self.integerCanvas = love.graphics.newCanvas(integerWidth, integerHeight)
      self.integerCanvas:setFilter("linear", "linear")
    end
  else
    self.wScaleX = self.scaleX
    self.wScaleY = self.scaleY

    self.wPosX = math.floor((windowWidth - integerWidth) / 2) + rotAdjX
    self.wPosY = math.floor((windowHeight - integerHeight) / 2) + rotAdjY
  end

  local joysticks = love.joystick.getJoysticks()

  for i, joystick in ipairs(joysticks) do
    print(joystick:getName(), joystick:isGamepad())
  end

  local theme = buildTheme(conf.theme, canvasWidth, canvasHeight)
  self.backgroundColor = theme.backgroundColor

  rocket = Rocket(theme, rules)
  rocket:load(conf.basePath)

  -- local fmts = love.graphics.getImageFormats()
  -- for k, v in pairs(fmts) do
  --   print(k, v)
  -- end
end

function LOVE:startRepeat(fn)
  -- sanity check
  self:stopRepeat()

  local repeatFn
  repeatFn = function()
    fn()
    self.repeatTimer = tick.delay(repeatFn, 0.05)
  end
  fn()
  self.repeatTimer = tick.delay(repeatFn, 0.25)
end

function LOVE:stopRepeat()
  if (self.repeatTimer ~= nil) then
    self.repeatTimer:stop()
    self.repeatTimer = nil
  end
end

local app = LOVE(love.arg.parseGameArguments(arg))

function love.textinput(t)
  rocket:textInputAdd(t)
end

function love.gamepadaxis(joystick, axis, value)
  -- sanity check
  app:stopRepeat()

  if (axis == "leftx") then
    if (value == 1) then
      app:startRepeat(
        function()
          rocket:selectPageDown()
        end
      )
    elseif (value == -1) then
      app:startRepeat(
        function()
          rocket:selectPageUp()
        end
      )
    end
  elseif (axis == "lefty") then
    if (value == 1) then
      app:startRepeat(
        function()
          rocket:selectDown()
        end
      )
    elseif (value == -1) then
      app:startRepeat(
        function()
          rocket:selectUp()
        end
      )

    elseif (value == 0) then
      app:stopRepeat()
    end
  end
end

function love.gamepadreleased()
  app:stopRepeat()
end

function love.gamepadpressed(joystick, button)
  -- sanity check
  app:stopRepeat()

  if (button == "dpup") then
    app:startRepeat(
      function()
        rocket:selectUp()
      end
    )
  elseif (button == "dpdown") then
    app:startRepeat(
      function()
        rocket:selectDown()
      end
    )
  elseif (button == "dpleft") then
    app:startRepeat(
      function()
        rocket:selectPageUp()
      end
    )
  elseif (button == "dpright") then
    app:startRepeat(
      function()
        rocket:selectPageDown()
      end
    )
  elseif (button == "a") then
    rocket:execute()
  elseif (button == "b") then
    rocket:goUp()
  end
end

function love.keyreleased()
  app:stopRepeat()
end

function love.keypressed(key)
  app:stopRepeat()

  if (key == "up") then
    app:startRepeat(
      function()
        rocket:selectUp()
      end
    )

  elseif (key == "down") then
    app:startRepeat(
      function()
        rocket:selectDown()
      end
    )

  elseif (key == "pageup") then
    app:startRepeat(
      function()
        rocket:selectPageUp()
      end
    )

  elseif (key == "pagedown") then
    app:startRepeat(
      function()
        rocket:selectPageDown()
      end
    )

  elseif (key == "home") then
    rocket:selectHome()

  elseif (key == "end") then
    rocket:selectEnd()

  elseif (key == "return") then
    rocket:execute()

  elseif (key == "backspace") then
    if (rocket:hasTextInput()) then
      rocket:textInputBackspace()
    else
      rocket:goUp()
    end

  elseif (key == "q" and love.keyboard.isDown("lctrl")) then
    love.event.quit()

  elseif (key == "escape") then
    rocket:textInputReset()

  end
end

local transparentColor = { 0, 0, 0, 0 }

function love.run()
  love.timer.step()

  local dt = 0

  -- Main loop time.
  return function()
    dt = love.timer.step()

    tick.update(dt)
    flux.update(dt)

    -- Process events.
    love.event.pump()
    for name, a, b, c, d, e, f in love.event.poll() do
      if (name == "quit") then
        if (not love.quit or not love.quit()) then
          return a or 0
        end
      end
      love.handlers[name](a, b, c, d, e, f)
    end

    if love.graphics.isActive() then
      app.canvas:renderTo(
        function()
          util.resetGraphics(transparentColor, "alpha")
          rocket:draw(dt)
        end
      )

      local currentCanvas = app.canvas

      if (app.integerCanvas ~= nil) then
        app.integerCanvas:renderTo(
          function()
            util.resetGraphics(transparentColor, "alpha", "premultiplied")
            love.graphics.draw(app.canvas, 0, 0, 0, app.scaleX, app.scaleY)
          end
        )

        currentCanvas = app.integerCanvas
      end

      util.resetGraphics(app.backgroundColor, "alpha", "premultiplied")
      love.graphics.draw(currentCanvas, app.wPosX, app.wPosY, app.rotation, app.wScaleX, app.wScaleY)

      love.graphics.present()
    end

    love.timer.sleep(0.001)
  end
end
