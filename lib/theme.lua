local ini = require("lib/ini")
local util = require("lib/util")
local BitmapFont = require("lib/bitmapfont")

local function computeThemeProp(source, width, height, offsetX, offsetY)
  local props = {
    color = util.convertColor(source.color),
    backgroundColor = util.convertColor(source.backgroundColor),
    radius = source.radius or 0
  }

  local padding = util.makeTRBL(source.padding)

  -- left / right | width
  -- todo: clamp

  local leftAmt = source.left or 0
  local rightAmt = source.right or 0
  local maxWidth = width - leftAmt - rightAmt

  if (source.width ~= nil) then
    local widthAmt = source.width + padding.left + padding.right
    props.width = math.min(maxWidth, widthAmt)

    if (source.right ~= nil) then -- if right + width ignore left
      props.left = width - props.width - rightAmt
    else
      props.left = leftAmt
    end
  else -- no width specified, max width
    props.width = maxWidth
    props.left = leftAmt
  end

  props.right = props.left + props.width

  -- top / bottom | height
  -- todo: clamp

  local topAmt = source.top or 0
  local bottomAmt = source.bottom or 0
  local maxHeight = height - topAmt - bottomAmt

  if (source.height ~= nil) then
    local heightAmt = source.height + padding.top + padding.bottom
    props.height = math.min(maxHeight, heightAmt)

    if (source.bottom ~= nil) then -- if bottom + height ignore top
      props.top = height - props.height - bottomAmt
    else
      props.top = topAmt
    end
  else -- no height specified, max height
    props.height = maxHeight
    props.top = topAmt
  end

  props.bottom = props.top + props.height

  -- todo: clamp
  props.innerHeight = props.height - padding.top - padding.bottom
  props.innerWidth = props.width - padding.left - padding.right
  props.innerLeft = props.left + padding.left
  props.innerRight = props.right - padding.right
  props.innerTop = props.top + padding.top
  props.innerBottom = props.bottom - padding.bottom

  if (offsetX ~= nil) then
    props.left = props.left + offsetX
    props.right = props.right + offsetX
    props.innerLeft = props.innerLeft + offsetX
    props.innerRight = props.innerRight + offsetX
  end

  if (offsetY ~= nil) then
    props.top = props.top + offsetY
    props.bottom = props.bottom + offsetY
    props.innerTop = props.innerTop + offsetY
    props.innerBottom = props.innerBottom + offsetY
  end

  return props
end


local function buildTheme(name, width, height)
  local themePath = util.join("themes", name, name .. '.ini')
  local source = ini.decode(love.filesystem.lines(themePath))

  local themeFontPath = util.join("themes", name, source.font)

  local theme = {}

  theme.backgroundColor = util.convertColor(source.backgroundColor)

  -- font
  theme.font = BitmapFont(themeFontPath)

  theme.lineHeight = theme.font.metrics.height

  -- breadcrumb
  source.breadcrumb.height = theme.lineHeight -- force height to 1 line
  theme.breadcrumb = computeThemeProp(source.breadcrumb, width, height)

  -- info
  source.info.height = theme.lineHeight -- force height to 1 line
  theme.info = computeThemeProp(source.info, width, height)

  theme.dateFormat = source.dateFormat

  -- list
  -- todo: list padding (innerHeight)
  local list = computeThemeProp(source.list, width, height)

  theme.pageSize = math.floor(list.height / theme.lineHeight)

  local actualListHeight = theme.pageSize * theme.lineHeight
  source.list.height = actualListHeight
  source.list.top = list.top + math.ceil((list.height - actualListHeight) / 2)
  source.list.bottom = nil

  theme.list = computeThemeProp(source.list, width, height)

  theme.file = {
    color = util.convertColor(source.file.color) --
  }

  theme.folder = {
    color = util.convertColor(source.folder.color) --
  }

  -- track
  if (source.track.top == nil) then
    source.track.top = theme.list.top
  end

  if (source.track.height == nil and source.track.bottom == nil) then
    source.track.height = theme.list.height
  end

  theme.track = computeThemeProp(source.track, width, height)

  theme.thumb = {
    radius = source.thumb.radius or 0,
    color = util.convertColor(source.thumb.color) --
  }

  theme.indicator = {
    left = source.indicator.left or 0,
    color = util.convertColor(source.indicator.color) --
  }

  -- selector
  theme.selector = computeThemeProp(source.selector, width, height)

  return theme
end

return buildTheme
