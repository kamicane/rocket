local love = require("love")
local ffi = require("ffi")
local Object = require("classic")
local flux = require("flux")

-- todo: solve path issues
local fzy = require("modules/fzy/lua/native")

local FSList = require("lib/fs-list")
local util = require("lib/util")

local ScrollBar = require("lib/scrollbar")
local glob = require("lib/globtopattern")

local Rocket = Object:extend()

function Rocket:new(theme, rules)
  self.scrollbar = ScrollBar()

  self.minIndex = 0
  self.maxIndex = -1
  self.selectedIndex = 0
  self.startIndex = 0

  self.items = {}
  self.fsList = nil

  self.theme = theme
  self.rules = rules

  self:textInputReset()

  self.tweens = {}
end

function Rocket:draw(dt)
  local theme = self.theme
  local font = theme.font

  if (theme.info.backgroundColor ~= nil) then
    love.graphics.setColor(theme.info.backgroundColor)
    love.graphics.rectangle("fill", theme.info.left, theme.info.top, theme.info.width, theme.info.height)
  end

  love.graphics.setColor(theme.info.color)

  local stringDate = os.date(theme.dateFormat)

  local textInput = self.textInput

  local filterText = "» " .. textInput .. "_"

  while (font:measure(filterText .. " " .. stringDate) > theme.info.innerWidth) do
    -- sanity check
    if (#textInput == 0) then
      break
    end

    filterText = "» " .. util.utf8shift(textInput) .. "_"
  end
  font:print(filterText, theme.info.innerLeft, theme.info.innerTop)

  font:printRight(stringDate, theme.info.innerRight, theme.info.innerTop)

  -- breadcrumb
  if (theme.breadcrumb.backgroundColor ~= nil) then
    love.graphics.setColor(theme.breadcrumb.backgroundColor)
    love.graphics.rectangle("fill", theme.breadcrumb.left, theme.breadcrumb.top, theme.breadcrumb.width, theme.breadcrumb.height)
  end

  -- breadcrumb
  love.graphics.setColor(theme.breadcrumb.color)

  local breadcrumbs = self:getBreadcrumbs()
  local bcFullText = "/" .. table.concat(breadcrumbs, "/")

  local bcFinalText = bcFullText

  while (font:measure(bcFinalText) > theme.breadcrumb.innerWidth) do
    table.remove(breadcrumbs, 1)
    if (#breadcrumbs == 0) then
      bcFinalText = "/…"
      break
    else
      bcFinalText = "/…/" .. table.concat(breadcrumbs, "/")
    end
  end

  font:print(bcFinalText, theme.breadcrumb.innerLeft, theme.breadcrumb.innerTop)

  if (#self.filtered == 0) then
    return
  end

  local tweens = self.tweens

  if (theme.list.backgroundColor ~= nil) then
    love.graphics.setColor(theme.list.backgroundColor)
    love.graphics.rectangle("fill", theme.list.left, theme.list.top, theme.list.width, theme.list.height)
  end

  -- selector
  if (theme.selector.backgroundColor ~= nil) then
    love.graphics.setColor(theme.selector.backgroundColor)
    love.graphics.rectangle("fill", theme.selector.left, self.selectorTop, theme.selector.width, theme.lineHeight)
  end

  -- indicator
  if (theme.indicator.color ~= nil) then
    love.graphics.setColor(theme.indicator.color)
    font:print("›", theme.indicator.left, self.selectorTop)
  end

  -- items

  -- scissor the list since it has extra items needed for scrolling
  love.graphics.setScissor(theme.list.left, theme.list.top, theme.list.width, theme.list.height)

  local startIndex = math.floor(tweens.scrollTop / theme.lineHeight)
  local endIndex = math.min(math.ceil((tweens.scrollTop + theme.list.height) / theme.lineHeight) - 1, self.maxIndex)

  for i = startIndex, endIndex do
    local listItem = self.filtered[i + 1] -- stupid lua
    local itemTop = util.round((i * theme.lineHeight) + theme.list.top - tweens.scrollTop)

    local basename = listItem.name

    local extWidth = 0

    if (listItem.type == "list") then
      love.graphics.setColor(theme.folder.color)
    else
      love.graphics.setColor(theme.file.color)
      if (listItem.ext ~= nil) then
        basename = listItem.name:match("^(.-)%.%w+$")
        if (basename == "" or basename == nil) then
          basename = listItem.name
        end
        -- file extension
        font:printRight(listItem.ext, theme.list.right, itemTop)
        extWidth = font:measure(" " .. listItem.ext)
      else
      end
    end

    local finalItemName = font:truncateRight(basename, theme.list.width - extWidth, "…")

    -- file / directory name
    font:print(finalItemName, theme.list.left, itemTop)
  end

  love.graphics.setScissor()

  -- scrollbar
  local scrollbar = self.scrollbar

  if (scrollbar:isVisible()) then
    -- track
    if (theme.track.backgroundColor ~= nil) then
      love.graphics.setColor(theme.track.backgroundColor)
      love.graphics.rectangle(
        "fill", theme.track.left, theme.track.top, theme.track.width, theme.track.height, theme.track.radius,
        theme.track.radius
      )
    end

    -- handle
    love.graphics.setColor(theme.thumb.color)
    love.graphics.rectangle(
      "fill", theme.track.left, theme.track.top + util.round(tweens.handleTop), theme.track.width,
        scrollbar.handleHeight, theme.thumb.radius, theme.thumb.radius
    )
  else

  end
end

function Rocket:itemToIndex(selectedItem)
  local selectedIndex

  if (selectedItem ~= nil) then
    for i = 1, #self.filtered do
      local item = self.filtered[i]
      if (selectedItem == item) then
        selectedIndex = i - 1
        break
      end
    end
  end

  if (selectedIndex == nil) then
    if (self.fsList.parent) then
      selectedIndex = 1
    else
      selectedIndex = 0
    end
  end

  return selectedIndex
end

function Rocket:updateList(selectedItem)
  -- reset animations after updating
  if (self.scrollTween) then
    self.scrollTween:stop()
    self.scrollTween = nil
  end
  self.tweens.handleTop = nil
  self.tweens.scrollTop = nil

  self.maxIndex = #self.filtered - 1

  local selectedIndex = self:itemToIndex(selectedItem)

  local theme = self.theme

  local windowSize = math.max(#self.filtered, theme.pageSize)
  self.scrollbar:update(windowSize, theme.pageSize, theme.track.height)

  self:select(selectedIndex)
end

function Rocket:filterList()
  if (self.textInput == "") then
    self.filtered = self.items
  else
    local scored = {}
    self.filtered = {}
    local startIndex = 1
    if (self.fsList.parent) then
      startIndex = 2
      table.insert(self.filtered, self.items[1])
    end
    for i = startIndex, #self.items do
      local item = self.items[i]
      local fzyMatch = fzy.has_match(self.textInput, item.name)
      if (fzyMatch) then
        local score = fzy.score(self.textInput, item.name)
        table.insert(scored, { item = item, score = score })
      end
    end

    table.sort(scored, util.fzySort)
    for i = 1, #scored do
      table.insert(self.filtered, scored[i].item)
    end
  end

  self:updateList(self.filtered[2] or self.filtered[1])
end

function Rocket:navigateToList(fsList)
  print("navigateToList", fsList.path)

  self.items = fsList:load()

  self.minIndex = 0

  self.selectedIndex = 0
  self.startIndex = 0

  self.fsList = fsList

  -- reset filters after navigating to a new dir
  self:textInputReset()
  self.filtered = self.items
  self:updateList(fsList.selectedItem)
end

function Rocket:load(rootPath)
  local rootList = FSList(rootPath)
  self:navigateToList(rootList)
end

function Rocket:execute()
  local item = self.filtered[self.selectedIndex + 1] -- thanks lua
  if (item == nil) then
    return
  end

  -- print(item.type, item.path)
  if (item.type == "list") then
    -- print(item.type)
    -- self:load(item.path)
    self:navigateToList(item)
  else
    local command
    local rules = self.rules

    for i = 1, #rules do
      local rule = rules[i]
      local pattern = glob(rule.glob)
      local match = item.path:match(pattern)
      if (match ~= nil) then
        command = rule.command
        break
      end
    end

    if (command ~= nil) then
      local itemFullPath = ('"%s"'):format(util.join(item.root, item.path))

      local executeString = command:format(itemFullPath)
      -- windows needs extra quotes
      if (ffi.os == "Windows") then
        executeString = ('"%s"'):format(executeString)
      end

      -- self.loading = item.name -- todo: loading screen. 1 frame of goodness.

      os.execute(executeString)
      -- after the thread is paused with os.execute, the event queue gets filled up.
      -- clear it before calling poll() (in run).
      love.event.clear()
    else
      print(item.path)
    end
  end
end

function Rocket:goUp()
  if (self.fsList ~= nil and self.fsList.parent ~= nil) then
    self:navigateToList(self.fsList.parent)
  end
end

function Rocket:getBreadcrumbs()
  if (self.fsList == nil) then
    return {}
  end

  return util.split(self.fsList.path)
end

function Rocket:select(selectedIndex)
  local theme = self.theme

  if (selectedIndex < self.minIndex) then
    selectedIndex = self.minIndex
  end
  if (selectedIndex > self.maxIndex) then
    selectedIndex = self.maxIndex
  end

  local numUp = math.floor(theme.pageSize / 2) - 1
  local startIndex = math.max(selectedIndex - numUp, 0)

  local endIndex = math.min(startIndex + theme.pageSize - 1, self.maxIndex)
  startIndex = math.max(endIndex - theme.pageSize + 1, 0)

  self.selectedIndex = selectedIndex
  self.startIndex = startIndex
  self.endIndex = endIndex

  if (self.fsList ~= nil) then
    local item = self.filtered[selectedIndex + 1]
    self.fsList.selectedItem = item
  end

  local tweens = self.tweens

  if (self.scrollTween) then
    self.scrollTween:stop()
    self.scrollTween = nil
  end

  self.scrollbar:scrollTo(startIndex)
  self.scrollTop = startIndex * theme.lineHeight
  self.selectorTop = (selectedIndex * theme.lineHeight) + theme.list.top - self.scrollTop

  if (tweens.handleTop == nil) then
    tweens.handleTop = self.scrollbar.handleTop
  end

  if (tweens.scrollTop == nil) then
    tweens.scrollTop = self.scrollTop
  end

  -- if (tweens.selectorTop == nil) then
  --   tweens.selectorTop = self.selectorTop
  -- end

  self.scrollTween = flux.to(
    tweens, 0.2, {
      handleTop = self.scrollbar.handleTop,
      scrollTop = self.scrollTop
      -- selectorTop = self.selectorTop
    }
  )
end

function Rocket:selectUp()
  self:select(self.selectedIndex - 1)
end

function Rocket:selectDown()
  self:select(self.selectedIndex + 1)
end

function Rocket:selectPageUp()
  self:select(self.selectedIndex - self.theme.pageSize)
end

function Rocket:selectPageDown()
  self:select(self.selectedIndex + self.theme.pageSize)
end

function Rocket:selectHome()
  self:select(0)
end

function Rocket:selectEnd()
  self:select(self.maxIndex)
end

-- todo
function Rocket:selectLetterUp()

end

-- todo
function Rocket:selectLetterDown()

end

-- text input

function Rocket:textInputUpdate()
  self:filterList()
end

function Rocket:textInputAdd(char)
  self.textInput = self.textInput .. char
  self:textInputUpdate()
end

function Rocket:textInputBackspace()
  self.textInput = util.utf8pop(self.textInput)
  self:textInputUpdate()
end

function Rocket:hasTextInput()
  return #self.textInput > 0
end

function Rocket:textInputReset()
  self.textInput = ""
end

return Rocket
