local Object = require("classic")
local util = require("lib/util")

local BitmapFont = Object:extend()

function BitmapFont:new(fontName)
  local fontData = require(fontName)
  local texture = fontData.texture
  self.metrics = fontData.metrics

  local parts = util.split(fontName)
  table.remove(parts)

  table.insert(parts, texture.file)
  local imagePath = util.join(unpack(parts))

  self.image = love.graphics.newImage(imagePath)

  local chars = {}

  for i = 1, #fontData.chars do
    local char = fontData.chars[i]
    chars[char.char] = {
      width = char.width,
      x = char.x,
      y = char.y,
      w = char.w,
      h = char.h,
      ox = char.ox,
      oy = char.oy,
      quad = love.graphics.newQuad(char.x, char.y, char.w, char.h, texture.width, texture.height)
    }
  end

  self.chars = chars
end

function BitmapFont:measure(text)
  local width = 0
  for p, c in util.utf8chars(text) do
    local char = self.chars[c] or self.chars[" "]
    width = width + char.width
  end

  return width
end

function BitmapFont:truncateRight(text, maxWidth, char)
  local baseText = text

  while (self:measure(text) > maxWidth) do
    if (#baseText == 0) then
      break
    end
    baseText = util.utf8pop(baseText)
    text = baseText .. char
  end

  return text
end

function BitmapFont:truncateLeft(text, maxWidth, char)
  local baseText = text

  while (self:measure(text) > maxWidth) do
    if (#baseText == 0) then
      break
    end
    baseText = util.utf8shift(baseText)
    text = baseText .. char
  end

  return text
end

-- print
function BitmapFont:print(text, x, y, maxWidth)
  local width = 0

  y = y + self.metrics.ascender

  for p, c in util.utf8chars(text) do
    local char = self.chars[c] or self.chars[" "]
    local totalWidth = width + char.width

    if (maxWidth ~= nil and totalWidth > maxWidth) then
      break
    end

    love.graphics.draw(self.image, char.quad, x + width + char.ox, y - char.oy)

    width = totalWidth
  end

  return width
end

-- print right
function BitmapFont:printRight(text, x, y, maxWidth)
  local width = 0

  y = y + self.metrics.ascender

  for p, c in util.utf8charsRight(text) do
    local char = self.chars[c] or self.chars[" "]
    local totalWidth = width + char.width

    if (maxWidth ~= nil and totalWidth > maxWidth) then
      break
    end

    width = totalWidth
    love.graphics.draw(self.image, char.quad, x - width + char.ox, y - char.oy)
  end

  return width
end

return BitmapFont
