local Object = require("classic")

local ScrollBar = Object:extend()

function ScrollBar:new()
  self.handleHeight = 0
  self.handleTop = 0
end

function ScrollBar:update(scrollSize, viewportSize, trackSize)
  self.scrollSize = scrollSize
  self.viewportSize = viewportSize
  self.trackSize = trackSize

  local shownPercent = viewportSize / scrollSize
  self.handleHeight = math.ceil(trackSize * shownPercent)
  self.scrollMax = scrollSize - viewportSize

  self.trackRatio = trackSize / scrollSize
end

function ScrollBar:isVisible()
  return (self.scrollSize ~= self.viewportSize)
end

function ScrollBar:scrollTo(scrollPos)
  -- sanity check
  if (scrollPos > self.scrollMax) then
    scrollPos = self.scrollMax
  end
  if (scrollPos < 0) then
    scrollPos = 0
  end

  self.handleTop = math.floor(self.trackRatio * scrollPos)
  -- print(self.trackRatio, scrollPos, self.trackRatio * scrollPos)
end

return ScrollBar
