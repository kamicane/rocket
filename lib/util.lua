local utf8 = require("utf8")

local util = {}

function util.split(path)
  local parts = {}
  for part in path:gmatch("[^/]+") do
    table.insert(parts, part)
  end
  return parts
end

-- naive path.join
function util.join(...)
  local joined = table.concat({ ... }, '/')
  local leading = joined:sub(1, 1) == '/'

  local parts = util.split(joined)

  joined = table.concat(parts, '/')
  return leading and '/' .. joined or joined
end

function util.utf8chars(text)
  return coroutine.wrap(
    function()
      for p, c in utf8.codes(text) do
        coroutine.yield(p, utf8.char(c))
      end
    end
  )
end

function util.utf8charsRight(text)
  local i = -1
  local en = #text + 1
  return coroutine.wrap(
    function()
      while (true) do
        local bos = utf8.offset(text, i, en)
        if (bos == nil) then
          break
        end
        local boe = utf8.offset(text, i + 1, en)
        coroutine.yield(bos, text:sub(bos, boe - 1))
        i = i - 1
      end
    end
  )
end

function util.utf8shift(text)
  local byteoffset = utf8.offset(text, 2)
  if (byteoffset ~= nil) then
    return text:sub(byteoffset)
  end
end

function util.utf8pop(text)
  local byteoffset = utf8.offset(text, -1)
  if (byteoffset ~= nil) then
    return text:sub(1, byteoffset - 1)
  end
end

function util.trim(str)
  return str:gsub("^%s*(.-)%s*$", "%1")
end

function util.convertColor(tColor)
  if (tColor == nil) then
    return nil
  end
  local red, green, blue, alpha = unpack(tColor)
  return { red / 255, green / 255, blue / 255, (alpha or 255) / 255 }
end

function util.makeTRBL(value)
  if (value == nil) then
    value = 0
  end
  local t = type(value)

  if (t == 'table') then
    if (#value == 4) then
      return { top = value[1], right = value[2], bottom = value[3], left = value[4] }
    elseif (#value == 2) then
      return { top = value[1], right = value[2], bottom = value[1], left = value[2] }
    end

  elseif (t == 'number') then
    return { top = value, right = value, bottom = value, left = value }
  end

  return nil
end

function util.fsSort(itemA, itemB)
  if (itemA.type == 'list' and itemB.type == 'item') then
    return true
  elseif (itemA.type == 'item' and itemB.type == 'list') then
    return false
  end

  if (itemA.type == 'item' and itemB.type == 'item') then
    return itemA.name < itemB.name
  end

  return itemA.name < itemB.name
end

function util.fzySort(itemA, itemB)
  if (itemA.score > itemB.score) then
    return true
  else
    return false
  end
end

function util.resetGraphics(backgroundColor, mode, alphamode)
  love.graphics.origin()
  love.graphics.clear(backgroundColor)
  -- very important!: reset color before drawing to canvas to have colors properly displayed
  love.graphics.setColor(1, 1, 1, 1)

  if (mode ~= nil) then
    love.graphics.setBlendMode(mode, alphamode)
  end
end

function util.round(x)
  return x >= 0 and math.floor(x + .5) or math.ceil(x - .5)
end

return util
