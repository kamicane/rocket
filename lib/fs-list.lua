local Object = require("classic")
local nfs = require("nativefs")

local util = require('lib/util')

local List = Object:extend()
local Item = Object:extend()

local cache = {}

local function getList(root, location, name, parent)
  local instance = cache[root][location]
  if (not instance or instance.type ~= 'list') then
    instance = List(root, location, name, parent)
    cache[root][location] = instance
  else
    instance.name = name
    instance.location = location
    instance.parent = parent
  end

  return instance
end

local function getItem(root, location, name, size)
  local instance = cache[root][location]
  if (not instance or instance.type ~= 'item') then
    instance = Item(root, location, name, size)
    cache[root][location] = instance
  else
    instance.name = name
    instance.location = location
    instance.size = size
  end

  return instance
end

function Item:new(root, location, name, size)
  self.type = 'item'

  self.root = root
  self.path = location
  local ext = name:match('(%.%w+)$')
  self.ext = ext

  self.name = name
  self.size = size
end

function List:new(root, location, name, parent)
  self.type = 'list'

  self.root = root
  self.name = name
  self.path = location
  self.parent = parent
end

function List:load()
  local itemInfos = nfs.getDirectoryItemsInfo(util.join(self.root, self.path))
  local items = {}

  for index, item in ipairs(itemInfos) do
    local location = util.join(self.path, item.name)

    if (item.type == 'directory' or item.type == 'symlink') then
      local fList = getList(self.root, location, item.name, self)
      table.insert(items, fList)
    elseif (item.type == 'file') then
      local fItem = getItem(self.root, location, item.name, item.size)
      table.insert(items, fItem)
    end
  end

  table.sort(items, util.fsSort)
  -- todo: cleanup
  if (self.parent) then
    table.insert(items, 1, getList(self.root, self.parent.path, '..', self.parent.parent))
  end

  return items
end

local function makeRootList(root, name)
  cache[root] = {}
  return getList(root, '/', name)
end

return makeRootList
