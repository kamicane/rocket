local Object = require("classic")

local TYPE_KEY = 0
local TYPE_COMMENT = 1

local util = {}

function util.toString(value)
  if (type(value) == "table" and value.__tostring == nil) then
    return util.tableToString(value)
  end

  return ("%s"):format(value)
end

function util.tableToString(t)
  local str = {}
  for i, value in ipairs(t) do
    table.insert(str, ("%s"):format(util.toString(value)))
  end

  return ("{ %s }"):format(table.concat(str, ", "))
end

-- string
function util.decodeString(value)
  value = value:match("^%s*(.-)%s*$") -- trim

  local stringItems = value:match("^{(.-)}$")

  if (stringItems ~= nil) then
    local t = {}
    for v in stringItems:gmatch("[^,]+") do
      local decoded = util.decodeString(v)
      table.insert(t, decoded)
    end
    return t
  end

  local quoted = value:match("^\"(.-)\"$")
  if (quoted) then
    value = quoted
  end

  if (value == "" or value == "nil") then
    return nil
  elseif (value == "true") then
    return true
  elseif (value == "false") then
    return false
  end

  local number = tonumber(value)
  if (number ~= nil) then
    return number
  end

  return value
end

local INISection = Object:extend()

function INISection:new(name, comment)
  self.name = name
  self.entriesIndex = {}
  self.data = {}
  if (comment ~= nil and #comment > 0) then
    self.comment = comment
  end
end

function INISection:entries()
  local i = 0
  local function iterator(tbl)
    i = i + 1
    local entry = tbl[i]
    if (entry ~= nil) then
      if (entry.type == TYPE_COMMENT) then
        return iterator(tbl)
      end
      return entry.key, entry.value
    end
  end

  return iterator, self.data, nil
end

function INISection:addComment(_one, _two)
  if (_two == nil) then
    local comment = _one
    if (comment ~= nil and #comment > 0) then
      table.insert(self.data, { type = TYPE_COMMENT, value = comment })
    end
  else
    local key, comment = _one, _two
    if (comment ~= nil and #comment > 0) then
      local entry = self.entriesIndex[key]

      if (entry == nil) then
        entry = { type = TYPE_KEY, key = key, comment = comment }
        table.insert(self.data, entry)
        self.entriesIndex[key] = entry
      else
        entry.comment = comment
      end

    end
  end
end

function INISection:set(key, value)
  local entry = self.entriesIndex[key]

  if (entry == nil) then
    entry = { type = TYPE_KEY, key = key, value = value }
    table.insert(self.data, entry)
    self.entriesIndex[key] = entry
  else
    entry.value = value
  end
end

function INISection:get(key)
  local entry = self.entriesIndex[key]
  if (entry ~= nil) then
    return entry.value
  end
  return nil
end

function INISection:encode()
  local result = {}

  if (self.name ~= nil) then
    local title
    if (self.comment ~= nil) then
      title = ("[%s] # %s"):format(self.name, self.comment)
    else
      title = ("[%s]"):format(self.name)
    end

    table.insert(result, title)
  end

  for _, entry in ipairs(self.data) do
    if (entry.type == TYPE_KEY) then
      local line
      if (entry.comment ~= nil) then
        line = ("%s = %s #%s"):format(entry.key, entry.value, entry.comment)
      else
        line = ("%s = %s"):format(entry.key, util.toString(entry.value))
      end
      table.insert(result, line)
    elseif (entry.type == TYPE_COMMENT) then
      table.insert(result, ("# %s"):format(entry.value))
    end
  end

  return table.concat(result, "\n")
end

function INISection:toTable()
  local result = {}
  for _, entry in ipairs(self.data) do
    if (entry.type == TYPE_KEY) then
      result[entry.key] = entry.value
    end
  end
  return result
end

function INISection:__tostring()
  return self:encode()
end

local INI = INISection:extend()

function INI:new()
  INISection.new(self)
  self.sectionIndex = {}
  self.sectionData = {}
end

function INI:sections()
  local i = 0
  local function iterator(tbl)
    i = i + 1
    local entry = tbl[i]
    if (entry ~= nil) then
      return entry.name, entry
    end
  end

  return iterator, self.sectionData, nil
end

function INI:section(name, comment)
  local section = self.sectionIndex[name]
  if (section == nil) then
    section = INISection(name, comment)
    self.sectionIndex[name] = section
    table.insert(self.sectionData, section)
  end

  return section
end

function INI:encode()
  local result = { INISection.encode(self), "" }

  for _, section in ipairs(self.sectionData) do
    if (#section.data > 0) then
      table.insert(result, section:encode())
      table.insert(result, "")
    end
  end

  return table.concat(result, "\n")
end

function INI:__tostring()
  return self:encode()
end

function INI:toTable()
  local result = INISection.toTable(self)
  for _, section in ipairs(self.sectionData) do
    if (#section.data > 0) then
      result[section.name] = section:toTable()
    end
  end

  return result
end

function INI:decode(lines)
  local currentSection = self

  for fline in lines do
    -- print(("%d line: '%s'"):format(i, fline))
    local line, comment = fline:match("^(.-)[;#]%s*(.-)%s*$")
    -- print(("line + comment: '%s', '%s'"):format(line, comment))

    local hasLine = false

    if (line == "") then -- only comment
      currentSection:addComment(comment)
    elseif (line == nil) then -- no comment
      line = fline
      hasLine = true
    else -- line + comment
      hasLine = true
    end

    if (hasLine) then
      local sectionName = line:match("^%s*%[(.+)%]%s*$")

      if (sectionName ~= nil) then
        -- print(("sectionName: '%s'"):format(sectionName))

        currentSection = self:section(sectionName, comment)
      else
        local key, value = line:match("^%s*(.-)%s*=%s*(.-)%s*$")
        if (key ~= nil) then
          -- print(("keyValue: '%s' = '%s'"):format(key, value))

          currentSection:set(key, util.decodeString(value), comment)
        end
      end
    end
  end
end

local function decode(lines)
  local ini = INI()
  ini:decode(lines)
  return ini:toTable()
end

local function encode(table)
  local ini = INI()

  for key, value in pairs(table) do
    local t = type(value)
    if (t == 'table') then
      local section = ini:section(key)
      for sKey, sValue in pairs(value) do
        section:set(sKey, sValue)
      end
    else
      ini:set(key, value)
    end
  end

  return ini:encode()
end

return { new = INI, encode = encode, decode = decode }
