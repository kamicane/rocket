local ffi = require('ffi')

-- Enable DPI awareness on Windows, so we don't get unwanted scaling
-- Inspired from how SDLPop does it
if (ffi.os == "Windows") then
  ffi.cdef [[
    int SetProcessDPIAware(void);
  ]]

  ffi.C.SetProcessDPIAware()
end

love.filesystem.setRequirePath("?.lua;?/init.lua;modules/?/?.lua;")

local identity = "rocket"

local ini = require("lib/ini")
local iniFile = ini.new()
local iniPath = identity .. ".ini"

-- must be set manually since we're inside conf.lua
love.filesystem.setIdentity(identity, false) -- false = try conf first then src

local info = love.filesystem.getInfo(iniPath)
if (info ~= nil) then
  iniFile:decode(love.filesystem.lines(iniPath))
else
  iniFile:decode(love.filesystem.lines("default.ini"))
  local success, message = love.filesystem.write(iniPath, iniFile:encode())
end

love.iniFile = iniFile -- is there a better way to transport values from conf ?

function love.conf(t)
  t.identity = identity
  t.appendidentity = false
  t.version = "11.3"
  t.console = true
  t.accelerometerjoystick = false
  t.externalstorage = false
  t.gammacorrect = false
  t.audio.mic = false
  t.audio.mixwithsystem = true

  t.window.title = identity:sub(1, 1):upper() .. identity:sub(2)
  t.window.icon = nil
  t.window.width = iniFile:get("windowWidth")
  t.window.height = iniFile:get("windowHeight")
  t.window.borderless = false
  t.window.resizable = false
  t.window.minwidth = 1
  t.window.minheight = 1
  t.window.fullscreen = iniFile:get("fullScreen")
  t.window.fullscreentype = iniFile:get("fullScreenType")
  t.window.vsync = 1
  t.window.msaa = 0
  t.window.depth = nil
  t.window.stencil = nil
  t.window.display = 1
  t.window.highdpi = false
  t.window.usedpiscale = false
  t.window.x = nil
  t.window.y = nil

  t.modules.audio = true
  t.modules.data = true
  t.modules.event = true
  t.modules.font = false
  t.modules.graphics = true
  t.modules.image = true
  t.modules.joystick = true
  t.modules.keyboard = true
  t.modules.math = true
  t.modules.mouse = true
  t.modules.physics = false
  t.modules.sound = true
  t.modules.system = true
  t.modules.thread = true
  t.modules.timer = true
  t.modules.touch = false
  t.modules.video = false
  t.modules.window = true
end
